/* 
 * File:   espiral.c
 * Author: Vinicius
 *
 * Created on 14 de Setembro de 2014, 11:09 
 * Ao informar um numero, as coordenadas imprime as cordenas
 * x,y do n-ésimo ponto. Sendo que o sistema anda em forma de caracol. 
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    int num, aux, nvezes = 1, cont=0, limite = 0, x=0, y=0, i;

    printf("\nDigite um numero: ");
    scanf("%d", &num);
    aux = num;
    if (num < 0) {//transforma o numero negativo em positivo
        aux = num * (-2);
        aux = aux + num;
    }
    while(limite<aux){
        //-------------------------------------------------------------
        if(cont==2 && limite<aux){
            cont=0;
            nvezes++;
        }
        if(cont<2 && limite<aux){//numero para cima
            for(i=0; i<nvezes && limite<aux; i++){
                y++;
                limite++;
            }
        }if(cont!=2){
            cont++;
        } 
        //-------------------------------------------------------------
        if(cont==2 && limite<aux){
            cont=0;
            nvezes++;
        }if(cont<2 && limite<aux){//numero para esquerda
            for(i=0; i<nvezes && limite<aux; i++){
                x--;
                limite++;
            }
        }if(cont!=2){
            cont++;
        }
        //-------------------------------------------------------------
        if(cont==2 && limite<aux){
            cont=0;
            nvezes++;
        }if(cont<2 && limite<aux){//numero para a baixo
            for(i=0; i<nvezes && limite<aux; i++){
                y--;
                limite++;
            }     
        }if(cont!=2){
            cont++;
        }
        //-------------------------------------------------------------
        if(cont>=2 && limite<aux){
            cont=0;
            nvezes++;
        }if(cont<2 && limite<aux){//numero para a direita
            for(i=0; i<nvezes && limite<aux; i++){
                x++;
                limite++;
            }     
        }if(cont!=2){
            cont++;
        }
        //-------------------------------------------------------------
    }
    printf("\nNumero %d, esta nas cordenadas:[%d] em X e [%d] em Y. \n", num, x, y);
    return (EXIT_SUCCESS);
}


